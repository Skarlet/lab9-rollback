import java.sql.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "test_user";
    static final String PASS = "pass";

    public static void main(String[] args) {
        LinkedList<Integer> employee_ids = new LinkedList<>(Arrays.asList(11, 21));
        LinkedList<Integer> amount = new LinkedList<>(Arrays.asList(200, 200));
        LinkedList<Integer> ids = new LinkedList<>(Arrays.asList(1,2));
        add_salaries(ids, employee_ids, amount);
    }

    public static void printRs(ResultSet rs) throws SQLException{
        //Ensure we start with first row
        rs.beforeFirst();
        while(rs.next()){
            //Retrieve by column name
            int id  = rs.getInt("id");
            int employee_id = rs.getInt("employee_id");
            int amount = rs.getInt("amount");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", EmployeeID: " + employee_id);
            System.out.println(", Amount: " + amount);
        }
        System.out.println();
    }

    public static void add_salaries(LinkedList<Integer> ids, LinkedList<Integer> employee_ids, LinkedList<Integer> amount) {

        Connection conn = null;
        Statement stmt = null;
        try{

            if (ids.size() != employee_ids.size() || employee_ids.size() != amount.size()) throw new Exception("Size of input do not match");
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            conn.setAutoCommit(false);


            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            for (int i = 0; i < ids.size(); i++ ) {
                System.out.println("Checking row....");
                String sql = "SELECT * FROM Salary WHERE id="+ids.get(i) + " OR employee_id=" + employee_ids.get(i);
                ResultSet rs = stmt.executeQuery(sql);
                if(rs.next()) throw new SQLException("Id is not correct!");
                System.out.println("Inserting row....");
                String SQL = "INSERT INTO Salary " +
                        "VALUES (" + Integer.toString(ids.get(i)) + "," +
                        Integer.toString(employee_ids.get(i)) + "," +
                        Integer.toString(amount.get(i)) + ")";
                stmt.executeUpdate(SQL);
            }

            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();

            String sql = "SELECT id, employee_id, amount FROM Salary";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            printRs(rs);
            rs.close();
            stmt.close();
            conn.close();
        }catch(SQLException se){
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try{
                if(conn!=null)
                    conn.rollback();
            }catch(SQLException se2){
                se2.printStackTrace();
            }//end try

        }catch(Exception e){
            //Handle errors for Class.forName
            e.printStackTrace();
        }finally{
            //finally block used to close resources
            try{
                if(stmt!=null)
                    stmt.close();
            }catch(SQLException se2){
            }
            try{
                if(conn!=null)
                    conn.close();
            }catch(SQLException se){
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Goodbye!");
    }

}
